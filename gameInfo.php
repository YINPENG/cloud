<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ROE</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
    h2{margin-left:auto;}
    h3{margin-left:auto;}
    .show{}
    </style>

</head>
<body>
<?php
    $cluster = Cassandra::cluster()
    ->withContactPoints('172.23.99.89','172.23.99.35')
    ->withPort(9042)
    ->build();
    $keyspace = 'cloudcomputing';
    $session = $cluster->connect($keyspace);
    $statement = new Cassandra\SimpleStatement(
    "SELECT * FROM game WHERE name='Ring Of Elysium';"
    );
    $future = $session->executeAsync($statement);
    $result = $future->get();
    foreach ($result as $row) { 
    ?>
        <div class="show">
        <h2>Game Name : </h2><h2><?php echo $row['name']?></h2>
        <h3>Publisher : </h3><h3><?php echo $row['publisher']?></h3>
        <h3>Game type : </h3><h3><?php echo $row['category']?></h3>
        <h3>Price : </h3><h3><?php echo $row['price']?></h3>
        <h3>Rating : </h3><h3><?php echo $row['rating']?></h3>
        </div>
    <?php } ?>
</body>
</html>